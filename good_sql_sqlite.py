import sqlite3

database_name = 'sqltest.db'


def main():
    db = connect(database_name)
    fetch(db)
    db.close()


def fetch(db):
    username = input("Please input a username: ")
    cursor = getCursor(db)
    cursor.execute(
        "SELECT * FROM login WHERE username = ?;", (username,))
    datas = cursor.fetchall()
    print(datas)


def connect(database_name):
    try:
        db_connection = sqlite3.connect(database_name)
        return db_connection
    except ConnectionError:
        print("Error when connecting to database!\n")


def getCursor(connection):
    return connection.cursor()


if __name__ == "__main__":
    main()
