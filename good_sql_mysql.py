import MySQLdb
import mysql.connector
from mysql.connector import Error

host = "localhost"
user = "root"
database = "SQLTest"


def main():
    db = connect(host, user, database)
    fetch(db)
    closeDb(db)


def fetch(db):
    username = input("Please input a username: ")
    cursor = db.cursor()
    cursor.execute("SELECT * FROM login WHERE username = %s;", (username,))
    datas = cursor.fetchone()
    print(datas)


def connect(host, user, database):
    try:
        db = mysql.connector.connect(
            host=host,
            user=user,
            database=database
        )
    except Error as e:
        print(e)
    finally:
        if db.is_connected():
            return db
        else:
            return None


def closeDb(db):
    if db.is_connected():
        db.close()


def getNum():
    while True:
        id = input("Please input a id: ")
        if isNumber(id):
            return id


def isNumber(value):
    try:
        float(value)
        return True
    except ValueError:
        print(ValueError)


if __name__ == "__main__":
    main()
