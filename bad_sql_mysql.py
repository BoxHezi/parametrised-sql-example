import MySQLdb
import mysql.connector
from mysql.connector import Error

host = "localhost"
user = "root"
database = "SQLTest"


def main():
    db = connect(host, user, database)
    fetch(db)
    closeDb(db)


def fetch(db):
    username = input("Please input a username: ")
    # print(username)
    query = "select * from login where username = '%s';" % username
    print("Query:", query)
    cursor = db.cursor()
    cursor.execute(query)
    datas = cursor.fetchone()
    print(datas)


def connect(host, user, database):
    try:
        db = mysql.connector.connect(
            host=host,
            user=user,
            database=database
        )
    except Error as e:
        print(e)
    finally:
        if db.is_connected():
            return db
        else:
            return None


def closeDb(db):
    if db.is_connected():
        db.close()


if __name__ == "__main__":
    main()
